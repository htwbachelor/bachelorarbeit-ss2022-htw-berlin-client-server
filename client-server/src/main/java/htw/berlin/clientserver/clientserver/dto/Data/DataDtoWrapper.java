package htw.berlin.clientserver.clientserver.dto.Data;

import java.util.ArrayList;
import java.util.List;

public class DataDtoWrapper {

    private List<DataDto> dataDtoList;

    public DataDtoWrapper() {
        dataDtoList = new ArrayList<>();
    }

    public DataDtoWrapper(List<DataDto> dataDtoList) {
        this.dataDtoList = dataDtoList;
    }

    public List<DataDto> getDataDtoList() {
        return dataDtoList;
    }

    public void setDataDtoList(List<DataDto> dataDtoList) {
        this.dataDtoList = dataDtoList;
    }
}
