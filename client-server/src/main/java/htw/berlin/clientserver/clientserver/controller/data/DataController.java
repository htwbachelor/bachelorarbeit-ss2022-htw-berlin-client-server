package htw.berlin.clientserver.clientserver.controller.data;

import htw.berlin.clientserver.clientserver.dto.Aggregate.ChartAggregate;
import htw.berlin.clientserver.clientserver.dto.Aggregate.GetChartAggregate;
import htw.berlin.clientserver.clientserver.dto.Chart.ChartDto;
import htw.berlin.clientserver.clientserver.dto.Chart.ChartLabel;
import htw.berlin.clientserver.clientserver.dto.Chart.Semesters;
import htw.berlin.clientserver.clientserver.dto.Data.DataDto;
import htw.berlin.clientserver.clientserver.dto.Data.DataDtoWrapper;
import htw.berlin.clientserver.clientserver.dto.Data.DataSummary;
import htw.berlin.clientserver.clientserver.dto.Data.Transcript;
import htw.berlin.clientserver.clientserver.dto.view.Chart;
import htw.berlin.clientserver.clientserver.dto.view.GradesChart;
import htw.berlin.clientserver.clientserver.service.data.IDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;

import org.springframework.security.oauth2.client.ClientAuthorizationRequiredException;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient;

@Controller
public class DataController {

    private static final Logger LOG = LoggerFactory.getLogger(DataController.class);
    private static final  String CREATE_CHART_SERVICE_URL = "http://localhost:8081/chart-composite/createData";
    private static final  String CHART_SERVICE_URL = "http://localhost:8081/chart-composite";
    private static final  String CHART_DATA_SERVICE_URL = "http://localhost:8081/chart-composite/chart-data";
    private static final  String SEMESTER_DATA_URL = "http://localhost:8081/chart-composite/getSemester";


    private final WebClient webClient;

    private final IDataService dataService;

    @Autowired
    public DataController(WebClient webClient, IDataService dataService) {
        this.webClient = webClient;
        this.dataService = dataService;
    }

    @GetMapping(value = "/addData")
    public String addNewData(@RequestParam String chartId,
                             @RequestParam String chartLabel,
                             Model model,
                             @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                             OAuth2AuthorizedClient authorizedClient){
        DataDto dataDto = new DataDto();
        String uri = "http://localhost:8081/chart-composite/getSemester/" + authorizedClient.getPrincipalName();
        try {
             List<String> semesters = this.webClient.get()
                    .uri(uri)
                    .attributes(oauth2AuthorizedClient(authorizedClient))
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToMono(new ParameterizedTypeReference<List<String>>() {})
                    .block();
             semesters = semesters != null ? semesters.stream().distinct().collect(Collectors.toList()) : new ArrayList<>();
            model.addAttribute("semesters", semesters);
            Semesters semesters1 = new Semesters(semesters);
            model.addAttribute("semesterList", semesters1);
        }catch (ClientAuthorizationRequiredException e){
            LOG.warn(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        model.addAttribute("chartId", chartId);
        model.addAttribute("data", dataDto);
        return "newData";
    }

    @PostMapping(value = "/addData")
    public String postNewData(@RequestParam String chartId,
                              @RequestParam String chartLabel,
                              @ModelAttribute DataDto dataDto,
                              @ModelAttribute Semesters semesters1,
                              @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                              OAuth2AuthorizedClient authorizedClient){

        if (chartLabel.equalsIgnoreCase("modules")){
            //postModuleData(chartId, chartLabel, dataDto, semesters1, authorizedClient);
        }else {
            GetChartAggregate chart = new GetChartAggregate();

            //Set data necessary to create DataSummery
            dataDto.setChartId(chartId);
            String dataId = dataService.generateDataId();
            dataDto.setDataId(dataId);
            dataDto.setStudentId(authorizedClient.getPrincipalName());
            Transcript transcript = new Transcript(dataDto.getSemester(), dataDto.getSemesterYear(), dataDto.getGrades(), 0, 0, 0.0);
            DataSummary dataSummary = new DataSummary(dataDto.getDataId(), dataDto.getChartId(), dataDto.getStudentId(), transcript, new ArrayList<>());

            try {
                this.webClient.post()
                        .uri(CREATE_CHART_SERVICE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(Mono.just(dataSummary), DataSummary.class)
                        .attributes(oauth2AuthorizedClient(authorizedClient))
                        .retrieve()
                        .bodyToMono(Void.class)
                        .block();
            }catch (ClientAuthorizationRequiredException e){
                LOG.warn(e.getMessage());
                throw new RuntimeException(e.getMessage());
            }
        }
        return "redirect:/home";
    }


    @GetMapping(value = "editeChart")
    public String getEditeChart(@RequestParam String chartId,
                                @ModelAttribute GradesChart chartModel,
                                @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                                OAuth2AuthorizedClient authorizedClient,
                                Model model){

        URI uri = dataService.buildDataURI(CHART_DATA_SERVICE_URL + "/{chartId}", chartId);

        try {
            ChartAggregate charts = this.webClient
                    .get()
                    .uri(uri)
                    .attributes(oauth2AuthorizedClient(authorizedClient))
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToMono(ChartAggregate.class)
                    .block();


            if (charts != null){
                //Transform to DataDto to send it to the presentation layer
                List<DataDto> data = dataService.convertFromDataSummeryListToDataDtoList(charts.getDataSummaryList());
                DataDtoWrapper dataDtoWrapper = new DataDtoWrapper(data);
                model.addAttribute("data", dataDtoWrapper);
            }else {
                //Send Empty DataDto to presentation
                model.addAttribute("data", new ArrayList<DataDto>());
            }

        }catch (WebClientResponseException.NotFound notFound){
            model.addAttribute("noChart", "Charts are empty");
        }

        return "editeChart";
    }

    @PostMapping(value = "editeChart")
    public String updateChart(@ModelAttribute DataDtoWrapper data,
                              @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                              OAuth2AuthorizedClient authorizedClient){

        List<DataDto> dataDtoList = data.getDataDtoList();
        ChartDto chartDto = new ChartDto(dataDtoList.get(0).getChartId(), dataDtoList.get(0).getStudentId(), null, null, null);

        // delete empty rows from grades list
        //convert DataDto to DataSummery, that the resource server can consume
        List<DataSummary> dataSummaryList = dataService.convertFromDataDtoListToDataSummeryList(dataDtoList);

        if (dataSummaryList != null){
            ChartAggregate chartAggregate = new ChartAggregate(chartDto, dataSummaryList);

            //Make update request to resource server
            try {
                URI uri = UriComponentsBuilder.fromUriString(CHART_SERVICE_URL).build().toUri();
                this.webClient
                        .patch()
                        .uri(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(Mono.just(chartAggregate), ChartAggregate.class)
                        .attributes(oauth2AuthorizedClient(authorizedClient))
                        .retrieve()
                        .bodyToMono(Void.class)
                        .block();
            }catch (ClientAuthorizationRequiredException e){
                LOG.warn(e.getMessage());
                throw new RuntimeException(e.getMessage());
            }

        }
        return "redirect:/home";
    }
}
