package htw.berlin.clientserver.clientserver.controller.home;

import htw.berlin.clientserver.clientserver.dto.Aggregate.ChartAggregate;
import htw.berlin.clientserver.clientserver.dto.Aggregate.GetChartAggregate;
import htw.berlin.clientserver.clientserver.dto.view.Chart;
import htw.berlin.clientserver.clientserver.service.home.IHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.net.URI;
import java.util.List;

import static org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient;

@Controller
public class HomeController {
//    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    private static final  String CHART_SERVICE_URL = "http://localhost:8081/chart-composite";
    private final WebClient webClient;
    private final IHomeService homeService;

    @Autowired
    public HomeController(WebClient webClient, IHomeService homeService) {
        this.webClient = webClient;
        this.homeService = homeService;
    }

    @GetMapping("/")
    public String root(){
        return "redirect:/home";
    }

    @GetMapping("/chartTest")
    public String test(){
        return "home";
    }

    @GetMapping("/home")
    public String index(@RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                                    OAuth2AuthorizedClient authorizedClient, Model model) {

            if (authorizedClient != null && !authorizedClient.getPrincipalName().isEmpty()) {
                URI uri = homeService.buildHomeURI(CHART_SERVICE_URL + "/{studentId}", authorizedClient.getPrincipalName());
                GetChartAggregate chart = new GetChartAggregate();
                try {
                    chart = this.webClient
                            .get()
                            .uri(uri)
                            .attributes(oauth2AuthorizedClient(authorizedClient))
                            .accept(MediaType.APPLICATION_JSON)
                            .retrieve()
                            .bodyToMono(GetChartAggregate.class)
                            .block();

                    List<ChartAggregate> chartAggregateList = chart.getChartAggregates();

                    List<Chart> charts = homeService.prepareCharts(chartAggregateList);

                    model.addAttribute("charts", charts);

                }catch (WebClientResponseException.NotFound notFound){
                    model.addAttribute("noChart", "You don't have charts");
                }

            }
//        return "home";
        return "home";
    }

}
