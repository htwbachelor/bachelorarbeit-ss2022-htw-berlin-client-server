package htw.berlin.clientserver.clientserver.controller.chart;

import htw.berlin.clientserver.clientserver.dto.Chart.ChartDto;
import htw.berlin.clientserver.clientserver.dto.Chart.ChartLabel;
import htw.berlin.clientserver.clientserver.dto.Chart.ChartType;
import htw.berlin.clientserver.clientserver.dto.Chart.Semesters;
import htw.berlin.clientserver.clientserver.service.chart.IChartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.ClientAuthorizationRequiredException;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

import static org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient;


@Controller
public class ChartController{

    private static final Logger LOG = LoggerFactory.getLogger(ChartController.class);
    private static final  String CHART_SERVICE_DELETE_URL = "http://localhost:8081/chart-composite";
    private static final  String CREATE_CHART_SERVICE_URL = "http://localhost:8081/chart-composite/createChart";

    private final WebClient webClient;

    private final IChartService chartService;


    @Autowired
    public ChartController(WebClient webClient, IChartService chartService) {
        this.webClient = webClient;
        this.chartService = chartService;
    }



    @GetMapping(value = "/create")
    public String getNewChart(Model model,
                              @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                              OAuth2AuthorizedClient authorizedClient){
        ChartDto chartDto = new ChartDto();
        model.addAttribute("chart", chartDto);
        return "newChart";
    }

    @PostMapping(value = "/create")
    private String createNewChart(@ModelAttribute ChartDto chartDto,
                                  @ModelAttribute Semesters semesters,
                                  @RequestParam String chartLabel,
                                  @RequestParam String chartType,
                                  @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                                  OAuth2AuthorizedClient authorizedClient){
        if (chartDto == null){
            chartDto.setChartLabel(chartLabel.equals("GRADES") ? ChartLabel.GRADES : ChartLabel.MODULES);
            chartDto.setChartType(chartType.equals("LINE") ? ChartType.LINE : ChartType.BAR);
        }
        chartDto.setChartId(chartService.generateChartId());
        chartDto.setStudentId(authorizedClient.getPrincipalName());

        // Send Chart to resource server
        try {
            this.webClient.post()
                    .uri(CREATE_CHART_SERVICE_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(Mono.just(chartDto), ChartDto.class)
                    .attributes(oauth2AuthorizedClient(authorizedClient))
                    .retrieve()
                    .bodyToMono(Void.class)
                    .block();
        }catch (ClientAuthorizationRequiredException e){
            LOG.warn(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        return "redirect:/home";
    }

    @GetMapping (value = "/deleteChart")
    private String deleteChart(@RequestParam String chartId,
                               @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                               OAuth2AuthorizedClient authorizedClient){
        URI uri = chartService.buildChartURI(CHART_SERVICE_DELETE_URL + "/{chartId}", chartId);
        try {
            this.webClient.delete()
                    .uri(uri)
                    .attributes(oauth2AuthorizedClient(authorizedClient))
                    .retrieve()
                    .bodyToMono(Void.class)
                    .block();
        }catch (ClientAuthorizationRequiredException e){
            LOG.warn(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        return "redirect:/home";
    }

    @PostMapping(value = "/addChildChart")
    private String createChild(@RequestParam String motherChartId,
                               @RequestParam String chartLabel,
                               @RequestParam String chartType,
                               @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                                   OAuth2AuthorizedClient authorizedClient){
        String chartId = chartService.generateChartId();
        String studentId = authorizedClient.getPrincipalName();
        ChartLabel chartL = chartLabel.equals("MODULE") ? ChartLabel.MODULES : ChartLabel.GRADES;
        ChartType chartT = chartType.equals("LINE") ? ChartType.LINE : ChartType.BAR;
        ChartDto chartDto = new ChartDto(chartId, studentId, chartL, chartT, motherChartId);
        // Send Chart to resource server
        try {
            this.webClient.post()
                    .uri(CREATE_CHART_SERVICE_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(Mono.just(chartDto), ChartDto.class)
                    .attributes(oauth2AuthorizedClient(authorizedClient))
                    .retrieve()
                    .bodyToMono(Void.class)
                    .block();
        }catch (ClientAuthorizationRequiredException e){
            LOG.warn(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        return "redirect:/home";

    }
}
