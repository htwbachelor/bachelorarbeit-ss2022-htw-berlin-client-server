function addField (tableId) {


    var myTable = document.getElementById("modgrad_table" + tableId);
    var currentIndex = myTable.rows.length;
    var currentRow = myTable.insertRow(-1);

    var vaildM= document.createElement("div");
    vaildM.setAttribute("class", "valid-feedback");
    vaildM.innerText = "Looks good!";

    var invaildM = document.createElement("div");
    invaildM.setAttribute("class", "invalid-feedback");
    invaildM.innerText = "Please add a module.";


    var module = document.createElement("input");
    module.setAttribute("class", "form-control");
    module.setAttribute("id", "dataDtoList" + tableId.toString() +".grades" + (currentIndex-1).toString() + ".module");
    module.setAttribute("name", "dataDtoList[" + tableId.toString() + "]" +".grades[" + (currentIndex-1).toString() + "].module");
    module.setAttribute("placeholder", "Module");
    module.setAttribute("required","");


    var vaildG= document.createElement("div");
    vaildG.setAttribute("class", "valid-feedback");
    vaildG.innerText = "Looks good!";

    var invaildG = document.createElement("div");
    invaildG.setAttribute("class", "invalid-feedback");
    invaildG.innerText = "Please add a module.";

    var grade = document.createElement("input");
    grade.setAttribute("class", "form-control");
    grade.setAttribute("id", "dataDtoList" + tableId.toString() +".grades" + (currentIndex-1).toString() + ".grade");
    grade.setAttribute("name", "dataDtoList[" + tableId.toString() + "]" +".grades[" + (currentIndex-1).toString() + "].grade");
    grade.setAttribute("placeholder", "Grade");
    grade.setAttribute("required","");

    var vaildC= document.createElement("div");
    vaildC.setAttribute("class", "valid-feedback");
    vaildC.innerText = "Looks good!";

    var invaildC = document.createElement("div");
    invaildC.setAttribute("class", "invalid-feedback");
    invaildC.innerText = "Please add a module.";

    var credits = document.createElement("input");
    credits.setAttribute("class", "form-control");
    credits.setAttribute("id", "dataDtoList" + tableId.toString() +".grades" + (currentIndex-1).toString() + ".credits");
    credits.setAttribute("name", "dataDtoList[" + tableId.toString() + "]" +".grades[" + (currentIndex-1).toString() + "].credits");
    credits.setAttribute("placeholder", "Credits");
    credits.setAttribute("required","");




    var deleteRowBox = document.createElement("button");
    deleteRowBox.setAttribute("id", "deleteRow")
    // deleteRowBox.setAttribute("id", "deleteRow" + (currentIndex).toString())
    deleteRowBox.setAttribute("type", "button");
    deleteRowBox.innerText = 'X';
     deleteRowBox.setAttribute("onclick", "RemoveRow()");
    deleteRowBox.setAttribute("class", "btn btn-danger");

    var currentCell = currentRow.insertCell(-1);
    currentCell.appendChild(module);
    currentCell.appendChild(vaildM);
    currentCell.appendChild(invaildM);

    currentCell = currentRow.insertCell(-1);
    currentCell.appendChild(grade);
    currentCell.appendChild(vaildG);
    currentCell.appendChild(invaildG);

    currentCell = currentRow.insertCell(-1);
    currentCell.appendChild(credits);
    currentCell.appendChild(vaildC);
    currentCell.appendChild(invaildC);

    currentCell = currentRow.insertCell(-1);
    currentCell.appendChild(deleteRowBox);
}
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();