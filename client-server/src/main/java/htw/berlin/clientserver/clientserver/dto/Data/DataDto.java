package htw.berlin.clientserver.clientserver.dto.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DataDto {
    private String dataId;
    private String chartId;
    private String studentId;
    private String semester;
    private int semesterYear;
    private List<ModuleGrade> grades;

    private Collection<String> childDataIds;

    public DataDto() {
        dataId = null;
        chartId = null;
        studentId = null;
        semester = null;
        semesterYear = 0;
        grades = new ArrayList<>();
        childDataIds = new ArrayList<>();
    }

    public DataDto(String dataId, String chartId, String studentId, String semester, int semesterYear, List<ModuleGrade> grades, Collection<String> childDataIds) {
        this.dataId = dataId;
        this.chartId = chartId;
        this.studentId = studentId;
        this.semester = semester;
        this.semesterYear = semesterYear;
        this.grades = grades;
        this.childDataIds = childDataIds;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getChartId() {
        return chartId;
    }

    public void setChartId(String chartId) {
        this.chartId = chartId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getSemesterYear() {
        return semesterYear;
    }

    public void setSemesterYear(int semesterYear) {
        this.semesterYear = semesterYear;
    }

    public List<ModuleGrade> getGrades() {
        return grades;
    }

    public void setGrades(List<ModuleGrade> grades) {
        this.grades = grades;
    }

    public Collection<String> getChildDataIds() {
        return childDataIds;
    }

    public void setChildDataIds(Collection<String> childDataIds) {
        this.childDataIds = childDataIds;
    }
}
