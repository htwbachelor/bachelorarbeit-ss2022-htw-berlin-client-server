package htw.berlin.clientserver.clientserver.service.home;

import htw.berlin.clientserver.clientserver.dto.Aggregate.ChartAggregate;
import htw.berlin.clientserver.clientserver.dto.view.Chart;
import htw.berlin.clientserver.clientserver.dto.view.GradesChart;
import htw.berlin.clientserver.clientserver.dto.view.ModuleChart;

import java.net.URI;
import java.util.List;

public interface IHomeService {

    URI buildHomeURI(String path, String attribute);
    List<Chart> prepareCharts(List<ChartAggregate> chartAggregateList);
}
