package htw.berlin.clientserver.clientserver.service.chart;

import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Service
public class ChartService implements IChartService{



    @Override
    public String generateChartId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public URI buildChartURI(String path, String attribute) {
        return UriComponentsBuilder.fromUriString(path).build(attribute);
    }
}
