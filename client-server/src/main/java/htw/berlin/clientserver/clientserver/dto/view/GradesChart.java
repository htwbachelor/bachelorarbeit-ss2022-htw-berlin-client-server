package htw.berlin.clientserver.clientserver.dto.view;

import htw.berlin.clientserver.clientserver.dto.Chart.ChartLabel;
import htw.berlin.clientserver.clientserver.dto.Chart.ChartType;
import htw.berlin.clientserver.clientserver.dto.Data.DataSummary;
import htw.berlin.clientserver.clientserver.dto.Data.Transcript;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GradesChart extends Chart{
    public GradesChart(String chartId, ChartType chartType, ChartLabel chartLabel, List<DataSummary> dataSummaryList) {
        super(chartId, chartType, chartLabel, dataSummaryList);
    }


    @Override
    protected void generateData(List<DataSummary> dataSummaryList){
        data = new ArrayList<>();
        data.add(List.of("Semester", "Grade"));
        if (dataSummaryList != null){
            List<Transcript> transcripts = dataSummaryList.stream().map(dataSummary -> dataSummary.getTranscript()).collect(Collectors.toList());
            chartX = transcripts.stream().map(transcript -> transcript.getSemesterLabel()).collect(Collectors.toList());
            chartY = transcripts.stream().map(transcript -> transcript.getAverage()).collect(Collectors.toList());
            transcripts.forEach(transcript -> {
                data.add(List.of(transcript.getSemester(), transcript.getAverage()));
            });
        }
    }
}
