package htw.berlin.clientserver.clientserver.service.data;

import htw.berlin.clientserver.clientserver.dto.Data.ModuleGrade;
import htw.berlin.clientserver.clientserver.dto.Data.DataDto;
import htw.berlin.clientserver.clientserver.dto.Data.DataSummary;
import htw.berlin.clientserver.clientserver.dto.Data.Transcript;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DataService implements IDataService{

    @Override
    public String generateDataId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public URI buildDataURI(String path, String attribute) {
        return UriComponentsBuilder.fromUriString(path).build(attribute);
    }

    @Override
    public List<DataDto> convertFromDataSummeryListToDataDtoList(List<DataSummary> dataSummaryList) {
        return  dataSummaryList.stream().map( dataSummary ->
                new DataDto(dataSummary.getDataId(), dataSummary.getChartId(), dataSummary.getStudentId(),
                        dataSummary.getTranscript().getSemester(),
                        dataSummary.getTranscript().getSemesterYear(),
                        (List<ModuleGrade>) dataSummary.getTranscript().getGrades(),
                        dataSummary.getChildDataIds())
        ).collect(Collectors.toList());
    }

    @Override
    public List<DataSummary> convertFromDataDtoListToDataSummeryList(List<DataDto> dataDtoList) {
        return dataDtoList.stream().map(dataDto -> {
            //Delete object from List if all its fields ary empty
            dataDto.getGrades().removeIf(grade-> (
                    grade.getGrade() == 0.0
                            && grade.getCredits() == 0
                            && (grade.getLabel() == null || grade.getLabel().isEmpty())
                            && grade.getModule() == null || grade.getModule().isEmpty()));

            Transcript transcript = new Transcript(dataDto.getSemester(), dataDto.getSemesterYear(), dataDto.getGrades(), 0, 0, 0.0);
            return new DataSummary(dataDto.getDataId(), dataDto.getChartId(), dataDto.getStudentId(), transcript, dataDto.getChildDataIds());
        }).collect(Collectors.toList());
    }
}
