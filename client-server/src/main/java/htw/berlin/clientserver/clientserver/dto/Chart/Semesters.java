package htw.berlin.clientserver.clientserver.dto.Chart;

import java.util.ArrayList;
import java.util.List;

public class Semesters {
    private List<String> semesters;

    public Semesters() {
        semesters = new ArrayList<>();
    }

    public Semesters(List<String> semesters) {
        this.semesters = semesters;
    }

    public List<String> getSemesters() {
        return semesters;
    }

    public void setSemesters(List<String> semesters) {
        this.semesters = semesters;
    }
}
