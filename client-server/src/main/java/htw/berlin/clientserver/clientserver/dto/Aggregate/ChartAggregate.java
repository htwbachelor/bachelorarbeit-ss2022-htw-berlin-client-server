package htw.berlin.clientserver.clientserver.dto.Aggregate;


import htw.berlin.clientserver.clientserver.dto.Chart.ChartDto;
import htw.berlin.clientserver.clientserver.dto.Data.DataSummary;

import java.util.ArrayList;
import java.util.List;

public class ChartAggregate {

    private final ChartDto chartSummary;
    private final List<DataSummary> dataSummaryList;

    public ChartAggregate() {
        chartSummary = null;
        dataSummaryList = new ArrayList<>();
    }

    public ChartAggregate(ChartDto chartSummary, List<DataSummary> dataSummaryList) {
        this.chartSummary = chartSummary;
        this.dataSummaryList = dataSummaryList;
    }

    public ChartDto getChartSummary() {
        return chartSummary;
    }

    public List<DataSummary> getDataSummaryList() {
        return dataSummaryList;
    }
}
