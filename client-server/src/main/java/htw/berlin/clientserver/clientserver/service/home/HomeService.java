package htw.berlin.clientserver.clientserver.service.home;

import htw.berlin.clientserver.clientserver.dto.Aggregate.ChartAggregate;
import htw.berlin.clientserver.clientserver.dto.Chart.ChartLabel;
import htw.berlin.clientserver.clientserver.dto.view.Chart;
import htw.berlin.clientserver.clientserver.dto.view.GradesChart;
import htw.berlin.clientserver.clientserver.dto.view.ModuleChart;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HomeService implements IHomeService{
    @Override
    public URI buildHomeURI(String path, String attribute) {
        return UriComponentsBuilder.fromUriString(path).build(attribute);
    }

    @Override
    public List<Chart> prepareCharts(List<ChartAggregate> chartAggregateList) {
        List<Chart> charts = new ArrayList<>();

        List<GradesChart> gradesCharts = getGradesCharts(chartAggregateList);
        gradesCharts.forEach(gradesChart -> charts.add(gradesChart));

        List<ModuleChart> modulesCharts = getModulesCharts(chartAggregateList);
        modulesCharts.forEach(moduleChart -> charts.add(moduleChart));

        return charts;
    }

    private List<ModuleChart> getModulesCharts(List<ChartAggregate> chartAggregateList) {
        List<ChartAggregate> chartAggregates = chartAggregateList.stream()
                .filter(chartAggregate -> chartAggregate.getChartSummary().getChartLabel() == ChartLabel.MODULES)
                .collect(Collectors.toList());

        List<ModuleChart> moduleCharts = chartAggregates.stream().map(chartAggregate ->
                new ModuleChart(
                        chartAggregate.getChartSummary().getChartId(),
                        chartAggregate.getChartSummary().getChartType(),
                        chartAggregate.getChartSummary().getChartLabel(),
                        chartAggregate.getDataSummaryList()
                )).collect(Collectors.toList());

        return moduleCharts;
    }

    private List<GradesChart> getGradesCharts(List<ChartAggregate> chartAggregateList) {
        List<ChartAggregate> chartAggregates = chartAggregateList.stream()
                .filter(chartAggregate -> chartAggregate.getChartSummary().getChartLabel() == ChartLabel.GRADES)
                .collect(Collectors.toList());

        List<GradesChart> gradesCharts = chartAggregates.stream().map(chartAggregate ->
                new GradesChart(
                        chartAggregate.getChartSummary().getChartId(),
                        chartAggregate.getChartSummary().getChartType(),
                        chartAggregate.getChartSummary().getChartLabel(),
                        chartAggregate.getDataSummaryList()
                )).collect(Collectors.toList());

        return gradesCharts;
    }
}
