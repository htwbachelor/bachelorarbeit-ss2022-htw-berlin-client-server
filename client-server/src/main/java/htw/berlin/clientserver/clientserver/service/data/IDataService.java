package htw.berlin.clientserver.clientserver.service.data;

import htw.berlin.clientserver.clientserver.dto.Data.DataDto;
import htw.berlin.clientserver.clientserver.dto.Data.DataSummary;

import java.net.URI;
import java.util.List;

public interface IDataService {
    String generateDataId();

    URI buildDataURI(String path, String attribute);

    List<DataDto> convertFromDataSummeryListToDataDtoList(List<DataSummary> dataSummaryList);
    List<DataSummary> convertFromDataDtoListToDataSummeryList(List<DataDto> dataDtoList);
}
