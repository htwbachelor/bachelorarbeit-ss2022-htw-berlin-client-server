package htw.berlin.clientserver.clientserver.dto.Data;

import java.util.ArrayList;
import java.util.List;

public class GlobalChartDataWrapper {
    private  List<GlobalChartData> globalChartDataList;
    private  List<GlobalChartData> studentDataList;


    public GlobalChartDataWrapper() {
        globalChartDataList = new ArrayList<>();
        studentDataList = new ArrayList<>();
    }

    public GlobalChartDataWrapper(List<GlobalChartData> globalChartDataList, List<GlobalChartData> studentDataList) {
        this.globalChartDataList = globalChartDataList;
        this.studentDataList = studentDataList;
    }


    public List<GlobalChartData> getGlobalChartDataList() {
        return globalChartDataList;
    }


    public List<GlobalChartData> getStudentDataList() {
        return studentDataList;
    }

    public void setGlobalChartDataList(List<GlobalChartData> globalChartDataList) {
        this.globalChartDataList = globalChartDataList;
    }

    public void setStudentDataList(List<GlobalChartData> studentDataList) {
        this.studentDataList = studentDataList;
    }
}
