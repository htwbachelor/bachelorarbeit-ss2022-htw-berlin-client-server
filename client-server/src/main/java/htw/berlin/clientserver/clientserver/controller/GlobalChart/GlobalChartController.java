package htw.berlin.clientserver.clientserver.controller.GlobalChart;

import htw.berlin.clientserver.clientserver.dto.Data.GlobalChartDataWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient;

@Controller
public class GlobalChartController {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalChartController.class);

    private static final String GET_GLOBAL_CHART = "http://localhost:8081/chart-composite/global-chart";


    private final WebClient webClient;

    @Autowired
    public GlobalChartController(WebClient webClient) {
        this.webClient = webClient;
    }

    @GetMapping("/globalChart")
    public String globalChart(Model model, @RegisteredOAuth2AuthorizedClient("chart-client-authorization-code")
                                   OAuth2AuthorizedClient authorizedClient){
        GlobalChartDataWrapper globalChartDataWrapper =  this.webClient.get()
                .uri(GET_GLOBAL_CHART + "/" +authorizedClient.getPrincipalName())
                .attributes(oauth2AuthorizedClient(authorizedClient))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(GlobalChartDataWrapper.class)
                .block();

        List<String> labels = globalChartDataWrapper.getStudentDataList().stream().map(gl->gl.getSemester()).collect(Collectors.toList());
        //get only the semesters that the student has
        List<Double> globalAverages = globalChartDataWrapper.getGlobalChartDataList().stream().filter(gl-> labels.contains(gl.getSemester())).map(gl-> gl.getAverage())
                .collect(Collectors.toList());
        List<Double> studentAverages = globalChartDataWrapper.getStudentDataList().stream().map(sl->sl.getAverage()).collect(Collectors.toList());
        model.addAttribute("labels", labels);
        model.addAttribute("globalAverages", globalAverages);
        model.addAttribute("studentAverages", studentAverages);

        return "globalChart";
    }
}
