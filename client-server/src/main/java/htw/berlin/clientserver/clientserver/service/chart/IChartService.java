package htw.berlin.clientserver.clientserver.service.chart;

import java.net.URI;

public interface IChartService {
    String generateChartId();
    URI buildChartURI(String path, String attribute);
}
